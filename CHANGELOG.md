# Changelog

All notable changes to this project will be documented in this file.

## [0.2.0] - 18/11/2020

### Changed

- Animations are now linked together. This will allow in the future the package to add animation when the reticle state change. You can look at the change from CLOSER to SENSING to get an idea.

## [0.1.2] - 13/10/2019

### Fixed

- Reticle redraw only if reticle state change. It prevents parent to recreate reticle when setState called

## [0.1.1] - 29/09/2019

### Changed

- Code formatting
- More detail to pubspec.yaml
- Memory optimization

## [0.1.0] - 22/09/2019

- Initial release for flutter reticle library
