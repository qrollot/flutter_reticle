# Reticle

A Flutter plugin for iOS and Android that provide reticle for camera according to [Material Design Object Detection: Live Camera](https://material.io/collections/machine-learning/object-detection-live-camera.html)

<img src="https://gitlab.com/qrollot/flutter_reticle/raw/master/source/images/sensing.gif" alt="Sensing" width="200"> <img src="https://gitlab.com/qrollot/flutter_reticle/raw/master/source/images/closer.gif" alt="Closer" width="200"> <img src="https://gitlab.com/qrollot/flutter_reticle/raw/master/source/images/searching.gif" alt="Searching" width="200"> <img src="https://gitlab.com/qrollot/flutter_reticle/raw/master/source/images/error.gif" alt="Error" width="200">

## Installation

Add to pubspec.yaml.

```yaml
dependencies:
  ...
  flutter_reticle: ^0.2.0
```

## Usage

import flutter_reticle.dart

```dart
import 'package:flutter_reticle/flutter_reticle.dart';
```

use the reticle widget specifying the state

```dart
Reticle(ReticleState.SENSING)
```

for the searching mode, you can specify the searching animation duration
in milliseconds

```dart
Reticle(ReticleState.SEARCHING, duration: 2000)
```

### The different states are

```dart
enum ReticleState {
    SENSING,
    SEARCHING,
    CLOSER,
    ERROR
}
```

for a more concrete example head to the example folder
