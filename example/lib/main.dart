import 'package:flutter/material.dart';
import 'package:flutter_reticle/flutter_reticle.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Reticle Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Reticle Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  ReticleState selectedState = ReticleState.SENSING;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[700],
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Stack(children: <Widget>[
          DropdownButton<ReticleState>(
            value: selectedState,
            style: TextStyle(color: Colors.blue),
            onChanged: (ReticleState newReticleState) {
              setState(() {
                selectedState = newReticleState;
              });
            },
            items: ReticleState.values.map<DropdownMenuItem<ReticleState>>(
                (ReticleState reticleState) {
              return DropdownMenuItem<ReticleState>(
                value: reticleState,
                child: Text(reticleState.toString()),
              );
            }).toList(),
          ),
          Center(
            child: Reticle(selectedState),
          )
        ]));
  }
}
