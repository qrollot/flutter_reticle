import 'package:flutter/material.dart';
import 'animation/reticle_animator.dart';
import 'dart:math' as math;

class ReticlePainter extends CustomPainter {
  ReticleAnimator reticleAnimator;

  Paint outerRingFillPaint;
  Paint outerRingStrokePaint;
  Paint innerRingStrokePaint;
  Paint ripplePaint;
  Paint progressRingStrokePaint;
  Paint minidotPaint;

  double outerRingFillRadius;
  double outerRingStrokeRadius;
  double innerRingStrokeRadius;
  int rippleSizeOffset;
  int rippleStrokeWidth;
  Color rippleColor;
  int rippleAlpha;

  ReticlePainter({this.reticleAnimator}) : super() {
    outerRingFillPaint = Paint()
      ..style = PaintingStyle.fill
      ..color = Color(0x1F000000);

    outerRingStrokePaint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 4.0
      ..strokeCap = StrokeCap.round
      ..color = Color(0x9AFFFFFF);

    innerRingStrokePaint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2.0
      ..strokeCap = StrokeCap.round
      ..color = Colors.white;

    // ripple
    rippleColor = Color(0x9AFFFFFF);
    ripplePaint = Paint()
      ..style = PaintingStyle.stroke
      ..color = rippleColor;

    // progress
    progressRingStrokePaint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 4.0
      ..strokeCap = StrokeCap.round
      ..color = Colors.white;

    // minidot
    minidotPaint = Paint()
      ..style = PaintingStyle.fill
      ..strokeCap = StrokeCap.round
      ..color = Color(0x9AFFFFFF);

    outerRingFillRadius = 22;
    outerRingStrokeRadius = 24;
    innerRingStrokeRadius = 14;
    rippleSizeOffset = 24;
    rippleStrokeWidth = 4;
    rippleAlpha = rippleColor.alpha;
  }

  @override
  void paint(Canvas canvas, Size size) {
    // outerRingFill
    if (reticleAnimator.displayOuterRingFill) {
      canvas.drawCircle(Offset(0, 0), outerRingFillRadius, outerRingFillPaint);
    }

    // innerRing
    if (reticleAnimator.displayInnerRing) {
      canvas.drawCircle(
        Offset(0, 0),
        reticleAnimator.getInnerRingStrokeRadius(),
        innerRingStrokePaint,
      );
    }

    // outerRing
    if (reticleAnimator.displayOuterRing) {
      canvas.drawCircle(
          Offset(0, 0), outerRingStrokeRadius, outerRingStrokePaint);
    }

    // ripple
    if (reticleAnimator.displayRipple) {
      // Draws the ripple to simulate the breathing animation effect.
      Paint paint = ripplePaint
        ..color = rippleColor.withAlpha(
            (rippleAlpha * reticleAnimator.getRippleAlphaScale()).toInt())
        ..strokeWidth =
            rippleStrokeWidth * reticleAnimator.getRippleStrokeWidthScale();

      double radius = outerRingStrokeRadius +
          rippleSizeOffset * reticleAnimator.getRippleSizeScale();
      canvas.drawCircle(Offset(0, 0), radius, paint);
    }

    // error
    if (reticleAnimator.displayError) {
      int r = 24;
      double baseAngle = math.pi / 10;
      for (double i = 0; i < 2 * math.pi / baseAngle; i += 1) {
        double x = r *
            math.cos(
                baseAngle * i + baseAngle * reticleAnimator.getErrorValue());
        double y = r *
            math.sin(
                baseAngle * i + baseAngle * reticleAnimator.getErrorValue());
        canvas.drawCircle(Offset(x, y), 2, minidotPaint);
      }
    }

    // search
    if (reticleAnimator.displaySearch) {
      canvas.drawArc(
        Rect.fromCenter(
          center: Offset(0, 0),
          width: outerRingStrokeRadius * 2,
          height: outerRingStrokeRadius * 2,
        ),
        -math.pi / 2,
        2 * math.pi * reticleAnimator.getSearchValue(),
        false,
        progressRingStrokePaint,
      );
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
