import 'package:flutter/material.dart';

class ReticleAnimator extends ChangeNotifier {
  static const int DURATION_ANIMATION = 1333;
  static const int DURATION_RIPPLE_FADE_IN_MS = 333;
  static const int DURATION_RIPPLE_FADE_OUT_MS = 500;
  static const int DURATION_RIPPLE_EXPAND_MS = 833;
  static const int DURATION_RIPPLE_STROKE_WIDTH_SHRINK_MS = 833;
  static const int START_DELAY_ANIMATION = 1167;
  static const int START_DELAY_RIPPLE_FADE_OUT_MS = 667;
  static const int START_DELAY_RIPPLE_EXPAND_MS = 333;
  static const int START_DELAY_RIPPLE_STROKE_WIDTH_SHRINK_MS = 333;
  static const int TOTAL_DURATION_ANIMATION =
      DURATION_ANIMATION + START_DELAY_ANIMATION;

  Animation rippleStrokeWidthAnimation,
      rippleSizeScaleAnimation,
      rippleFadeInAnimator,
      rippleFadeOutAnimator,
      innerRingStrokeRadiusAnimator;
  AnimationController rippleController,
      innerRingController,
      errorController,
      searchController;

  // ripple
  double rippleAlphaScale = 0.0;
  double rippleSizeScale = 0.0;
  double rippleStrokeWidthScale = 1.0;
  bool displayRipple = false;

  // outerRingFill
  bool displayOuterRingFill = false;

  // outerRing
  bool displayOuterRing = false;

  // innerRing
  static const double maxInnerRingStrokeRadius = 14.0;
  static const double minInnerRingStrokeRadius = 6.0;
  double innerRingStrokeRadius = 14.0;
  bool displayInnerRing = false;

  // error
  bool displayError = false;

  // search
  bool displaySearch = false;
  int duration; // the animation duration in ms

  ReticleAnimator(TickerProvider ticker, {this.duration}) {
    if (this.duration == null) this.duration = 3000;

    // Controllers
    this.rippleController = AnimationController(
      duration: Duration(milliseconds: TOTAL_DURATION_ANIMATION),
      vsync: ticker,
    );

    this.innerRingController = AnimationController(
      duration: Duration(milliseconds: 200),
      vsync: ticker,
    );

    this.errorController = AnimationController(
        duration: Duration(milliseconds: 500), vsync: ticker)
      ..addListener(() {
        notifyListeners();
      });

    this.searchController = AnimationController(
        duration: Duration(milliseconds: this.duration), vsync: ticker)
      ..addListener(() {
        notifyListeners();
      });

    // Animations
    // ripple fade in
    rippleFadeInAnimator = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: this.rippleController,
        curve: Interval(
          START_DELAY_ANIMATION / TOTAL_DURATION_ANIMATION,
          (START_DELAY_ANIMATION + DURATION_RIPPLE_FADE_IN_MS) /
              TOTAL_DURATION_ANIMATION,
          curve: Curves.linear,
        ),
      ),
    );
    rippleFadeInAnimator.addListener(() {
      if (this.rippleController.value <=
          (START_DELAY_ANIMATION + DURATION_RIPPLE_FADE_IN_MS) /
              TOTAL_DURATION_ANIMATION) {
        this.rippleAlphaScale = rippleFadeInAnimator.value;
        notifyListeners();
      }
    });

    // ripple fade out
    rippleFadeOutAnimator = Tween(begin: 1.0, end: 0.0).animate(
      CurvedAnimation(
        parent: this.rippleController,
        curve: Interval(
          (START_DELAY_ANIMATION + START_DELAY_RIPPLE_FADE_OUT_MS) /
              TOTAL_DURATION_ANIMATION,
          1.0,
          curve: Curves.linear,
        ),
      ),
    );
    rippleFadeOutAnimator.addListener(() {
      if (this.rippleController.value >
          (START_DELAY_ANIMATION + START_DELAY_RIPPLE_FADE_OUT_MS) /
              TOTAL_DURATION_ANIMATION) {
        this.rippleAlphaScale = rippleFadeOutAnimator.value;
        notifyListeners();
      }
    });

    // ripple size scale
    rippleSizeScaleAnimation = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: rippleController,
        curve: Interval(
          (START_DELAY_ANIMATION + START_DELAY_RIPPLE_EXPAND_MS) /
              TOTAL_DURATION_ANIMATION,
          (START_DELAY_ANIMATION +
                  START_DELAY_RIPPLE_EXPAND_MS +
                  DURATION_RIPPLE_EXPAND_MS) /
              TOTAL_DURATION_ANIMATION,
          curve: Curves.fastOutSlowIn,
        ),
      ),
    );
    rippleSizeScaleAnimation.addListener(() {
      this.rippleSizeScale = rippleSizeScaleAnimation.value;
      notifyListeners();
    });

    // ripple stroke width controller
    rippleStrokeWidthAnimation = Tween(begin: 1.0, end: 0.5).animate(
      CurvedAnimation(
        parent: this.rippleController,
        curve: Interval(
          (START_DELAY_ANIMATION + START_DELAY_RIPPLE_STROKE_WIDTH_SHRINK_MS) /
              TOTAL_DURATION_ANIMATION,
          (START_DELAY_ANIMATION +
                  START_DELAY_RIPPLE_STROKE_WIDTH_SHRINK_MS +
                  DURATION_RIPPLE_STROKE_WIDTH_SHRINK_MS) /
              TOTAL_DURATION_ANIMATION,
          curve: Curves.fastOutSlowIn,
        ),
      ),
    );
    rippleStrokeWidthAnimation.addListener(() {
      this.rippleStrokeWidthScale = rippleStrokeWidthAnimation.value;
      notifyListeners();
    });

    // innerRingStrokeRadius
    innerRingStrokeRadiusAnimator = Tween(
      begin: maxInnerRingStrokeRadius,
      end: minInnerRingStrokeRadius,
    ).animate(
      CurvedAnimation(
        parent: this.innerRingController,
        curve: Curves.linear,
      ),
    );
    innerRingStrokeRadiusAnimator.addListener(() {
      this.innerRingStrokeRadius = innerRingStrokeRadiusAnimator.value;
      notifyListeners();
    });
  }

  double getRippleAlphaScale() {
    return this.rippleAlphaScale;
  }

  double getRippleSizeScale() {
    return this.rippleSizeScale;
  }

  double getRippleStrokeWidthScale() {
    return this.rippleStrokeWidthScale;
  }

  double getInnerRingStrokeRadius() {
    return this.innerRingStrokeRadius;
  }

  double getErrorValue() {
    return this.errorController.value;
  }

  double getSearchValue() {
    return this.searchController.value;
  }

  dispose() {
    this.rippleController.dispose();
    this.innerRingController.dispose();
    super.dispose();
  }

  closer() {
    // ripple
    this.rippleController.stop();
    this.rippleController.reset();
    this.displayRipple = false;

    // outerRingFill
    this.displayOuterRingFill = true;

    // outerRing
    this.displayOuterRing = true;

    // innerRing
    this.displayInnerRing = true;
    this.innerRingController.forward();

    // error
    this.errorController.stop();
    this.displayError = false;

    // search
    this.displaySearch = false;

    notifyListeners();
  }

  error() {
    // ripple
    this.rippleController.stop();
    this.rippleController.reset();
    this.displayRipple = false;

    // outerRingFill
    this.displayOuterRingFill = false;

    // outerRing
    this.displayOuterRing = false;

    // innerRing
    this.displayInnerRing = false;

    // error
    this.errorController.repeat();
    this.displayError = true;

    // search
    this.displaySearch = false;

    notifyListeners();
  }

  ripple() {
    // ripple
    this.rippleController.repeat();
    this.displayRipple = true;

    // outerRingFill
    this.displayOuterRingFill = true;

    // outerRing
    this.displayOuterRing = true;

    // innerRing
    this.displayInnerRing = true;
    this.innerRingController.reverse();

    // error
    this.errorController.stop();
    this.displayError = false;

    // search
    this.displaySearch = false;

    notifyListeners();
  }

  search() {
    // ripple
    this.rippleController.stop();
    this.rippleController.reset();
    this.displayRipple = false;

    // outerRingFill
    this.displayOuterRingFill = true;

    // outerRing
    this.displayOuterRing = true;

    // innerRing
    this.displayInnerRing = true;
    this.innerRingController.reverse();

    // error
    this.errorController.stop();
    this.displayError = false;

    // search
    this.displaySearch = true;
    this.searchController.reset();
    this.searchController.forward();

    notifyListeners();
  }
}
