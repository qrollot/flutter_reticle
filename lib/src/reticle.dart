import 'package:flutter/material.dart';

import 'animation/reticle_animator.dart';
import 'reticle_painter.dart';
import 'reticle_state.dart';

class Reticle extends StatefulWidget {
  final ReticleState state;
  final int duration;

  Reticle(this.state, {this.duration, Key key}) : super(key: key);

  @override
  _ReticleState createState() => _ReticleState();
}

class _ReticleState extends State<Reticle> with TickerProviderStateMixin {
  ReticleAnimator reticleAnimator;
  Map<ReticleState, dynamic> actions;
  ReticleState _state;

  @override
  void initState() {
    super.initState();

    this.reticleAnimator = ReticleAnimator(this, duration: widget.duration);

    actions = {
      ReticleState.CLOSER: this.reticleAnimator.closer,
      ReticleState.ERROR: this.reticleAnimator.error,
      ReticleState.SEARCHING: this.reticleAnimator.search,
      ReticleState.SENSING: this.reticleAnimator.ripple,
    };

    this.updateReticle(widget.state);

    this.reticleAnimator.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
        painter: ReticlePainter(reticleAnimator: this.reticleAnimator));
  }

  @override
  void didUpdateWidget(Reticle oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.state != _state) this.updateReticle(widget.state);
  }

  @override
  void dispose() {
    this.reticleAnimator.dispose();
    super.dispose();
  }

  void updateReticle(ReticleState state) {
    this.actions[widget.state]();

    _state = widget.state;
  }
}
